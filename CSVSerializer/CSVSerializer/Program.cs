﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;

namespace CSVSerializer
{
	class Program
	{
		static void Main(string[] args)
		{
			var f = new F().Get();
			var serializedSimpleObj = Serializer.Serialize(f);
			Console.WriteLine(serializedSimpleObj);
			Console.WriteLine();
			var testObj = new List<F> {f.Get(), f.Get1(), f.Get2()};
			var serializedList = Serializer.Serialize(testObj);
			Console.WriteLine(serializedList);
			var deserializedObj = Serializer.Deserialize<F>(serializedSimpleObj);
			var deserializedList = Serializer.Deserialize<List<F>>(serializedList);
			Console.WriteLine();
			TestPerfomance();
		}

		static void TestPerfomance()
		{
			var f = new F().Get();
			var testObj = new List<F> { f.Get(), f.Get1(), f.Get2() };
			string jsonString = null;
			string csvString = null;
			List<F> json, csv;
			var watch = new Stopwatch();
			watch.Start();
			for (int i = 0; i < 10000; ++i)
			{
				jsonString = JsonConvert.SerializeObject(testObj);
			}

			watch.Stop();
			Console.WriteLine("Newtonsoft 10000 iterations serialize = " + (watch.ElapsedMilliseconds));

			watch.Reset();
			watch.Start();
			for (int i = 0; i < 10000; ++i)
			{
				csvString = Serializer.Serialize(testObj);
			}

			watch.Stop();
			Console.WriteLine("Our serializer 10000 iterations serialize = " + (watch.ElapsedMilliseconds));

			watch.Reset();
			watch.Start();
			for (int i = 0; i < 10000; ++i)
			{
				json = JsonConvert.DeserializeObject<List<F>>(jsonString);
			}

			watch.Stop();
			Console.WriteLine("Newtonsoft 10000 iterations deserialize = " + (watch.ElapsedMilliseconds));

			watch.Reset();
			watch.Start();
			for (int i = 0; i < 10000; ++i)
			{
				csv = Serializer.Deserialize<List<F>>(csvString);
			}

			watch.Stop();
			Console.WriteLine("Our serializer 10000 iterations deserialize = " + (watch.ElapsedMilliseconds));
		}
	}

	class F
	{
		int i1, i2, i3, i4, i5; 
		public F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
		public F Get1() => new F() { i1 = 12, i2 = 21, i3 = 33, i4 = 44, i5 = 54 };
		public F Get2() => new F() { i1 = 0, i2 = 2, i3 = 44, i4 = 1, i5 = 8 };
	}

	
}
