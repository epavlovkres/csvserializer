﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CSVSerializer
{
	public static class Serializer
	{
		private const char Separator = ';';

		public static string Serialize(object obj)
		{
			var type = obj.GetType();
			if (IsPrimitiveType(type))
				ToString(obj);
			if (!IsEnumerableType(type))
			{
				var dict = MapToDictionary(obj).ToDictionary(p => p.Key, p => new List<string>() { p.Value });
				return SerializeDictionary(dict);
			}

			var dictionary = new Dictionary<string, List<string>>();
			foreach (var o in obj as IEnumerable)
			{
				var dict = MapToDictionary(o);
				foreach (var keyValuePair in dict)
				{
					if (dictionary.ContainsKey(keyValuePair.Key))
					{
						dictionary[keyValuePair.Key].Add(keyValuePair.Value);
					}
					else
					{
						dictionary[keyValuePair.Key] = new List<string>() {keyValuePair.Value};
					}
				}
			}

			return SerializeDictionary(dictionary);
		}

		public static T Deserialize<T>(string str)
		{
			var type = typeof(T);
			var dict = DeserializeToDictionary(str);
			if (IsEnumerableType(type))
			{
				var obj = (T) Activator.CreateInstance(type);
				if (!IsGenericList(type))
					return obj; // non list enumerables not supported
				var elementType = GetEnumerableElementType<T>();
				var elements = DeserializeDictionary(dict, elementType);
				var addMethod = type.GetMethod("Add");
				elements.ForEach(e =>
				{
					addMethod.Invoke(obj, new[] { e });
				});
				
				return obj;
			}

			return (T)DeserializeDictionary(dict, type)?[0];
		}

		private static bool IsGenericList(Type type)
		{
			return type.IsGenericType 
			       && type.GetGenericTypeDefinition() == typeof(List<>);
		}

		private static Type GetEnumerableElementType<T>()
		{
			Type elementType = null;
			var type = typeof(T);
			var interfaces = type.GetInterfaces();
			foreach (Type i in interfaces)
				if (i.IsGenericType && i.GetGenericTypeDefinition().Equals(typeof(IEnumerable<>)))
				{
					elementType = i.GetGenericArguments()[0];
					break;
				}

			return elementType;
		}

		private static List<object> DeserializeDictionary(Dictionary<string, List<string>> dict, Type type)
		{
			var deserialized = new List<object>();
			GetFieldProps(type, out var fields, out var props);
			foreach (var keyValuePair in dict)
			{
				var field = fields.FirstOrDefault(f => f.Name == keyValuePair.Key);
				var prop = props.FirstOrDefault(p => p.Name == keyValuePair.Key);
				if (field == null && prop == null)
					continue;
				var valueList = keyValuePair.Value;
				while (valueList.Count > deserialized.Count)
					deserialized.Add(Activator.CreateInstance(type));
				var fieldType = field == null ? prop.PropertyType : field.FieldType;
				for (int i = 0; i < valueList.Count; ++i)
				{
					var value = ConvertStringTo(valueList[i], fieldType);
					if (field == null)
						prop.SetValue(deserialized[i], value);
					else
						field.SetValue(deserialized[i], value);
				}
			}

			return deserialized;
		}

		private static object ConvertStringTo(string str, Type type)
		{
			if (!IsPrimitiveType(type))
				return null;
			return Convert.ChangeType(str, type);
		}

		private static bool IsPrimitiveType(Type t)
		{
			return t.IsPrimitive || (t == typeof(string));
		}

		private static bool IsEnumerableType(Type t)
		{
			return typeof(IEnumerable).IsAssignableFrom(t);
		}

		private static Dictionary<string, string> MapToDictionary(object obj)
		{
			GetFieldProps(obj.GetType(), out var fields, out var props);
			return fields.ToDictionary(f => f.Name, f => f.GetValue(obj))
				.Concat(props.ToDictionary(p => p.Name, p => p.GetValue(obj)))
				.ToDictionary(p => p.Key, p => ToString(p.Value));
		}
		

		private static string ToString(object obj)
		{
			return '"' + obj.ToString().Replace("\"", "\"\"") + '"';
		}

		private static void GetFieldProps(Type type, out FieldInfo[] fields, out PropertyInfo[] props)
		{
			fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
			props = type.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
		}

		private static string SerializeDictionary(Dictionary<string, List<string>> dict)
		{
			string tableHeader = "";
			string[] tableRow = new string[dict.First().Value.Count];
			foreach (var keyValuePair in dict)
			{
				tableHeader += keyValuePair.Key + Separator;
				for (int i = 0; i < keyValuePair.Value.Count; ++i)
				{
					tableRow[i] += keyValuePair.Value[i] + Separator;
				}
			}

			var serialized = tableHeader.Substring(0, tableHeader.Length - 1) + '\n';
			foreach (var row in tableRow)
			{
				serialized += row.Substring(0, row.Length - 1) + '\n';
			}

			return serialized;
		}

		private static Dictionary<string, List<string>> DeserializeToDictionary(string str)
		{
			var lines = str.Split('\n');
			var keys = lines[0].Split(Separator);
			var dict = new Dictionary<string, List<string>>();
			foreach (var key in keys)
			{
				dict[key] = new List<string>();
			}
			for (int i = 1; i < lines.Length; ++i)
			{
				var line = lines[i];
				int paramIdx = 0;
				int curIdx = 0;
				while (line.Length > curIdx)
				{
					if (line[curIdx] == '"')
					{
						var nextQuoteIndex = line.IndexOf('"', ++curIdx);
						while (nextQuoteIndex != -1 && nextQuoteIndex + 1 < line.Length && line[nextQuoteIndex + 1] == '"')
							nextQuoteIndex = line.IndexOf('"', nextQuoteIndex + 1);
						if (nextQuoteIndex == -1)
							nextQuoteIndex = line.Length;
						dict[keys[paramIdx]].Add(line.Substring(curIdx, nextQuoteIndex - curIdx));
						curIdx = nextQuoteIndex + 2;
					}
					else
					{
						var separatorIdx = line.IndexOf(Separator, curIdx);
						dict[keys[paramIdx]].Add(line.Substring(curIdx, separatorIdx - curIdx));
						curIdx = separatorIdx + 1;
					}
					++ paramIdx;
				}
			}

			return dict;
		}
	}

	interface IObjectField
	{
		void SetValue(object obj, object value);
		object GetValue(object obj);
		Type GetType();
	}
}
